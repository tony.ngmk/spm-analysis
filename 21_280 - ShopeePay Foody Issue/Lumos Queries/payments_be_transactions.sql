WITH pmt AS (
    SELECT payment_id, status, channel_id, amount, transaction_id, ctime
    FROM shopee.shopee_payment_module_payment_id_db__payment_v2_tab 
    WHERE payment_type = 4 --payment type = PAYMENT
    AND DATE(from_unixtime(ctime-(3600))) BETWEEN DATE '2021-08-02' AND DATE '2021-08-23'
    AND channel_id = 8001400 -- ID-ShopeePay
), 
txn AS (
    SELECT client_id, transaction_id FROM shopee.shopee_payment_module_transaction_id_db__transaction_v2_tab
), 
prov AS (
    SELECT channel_id, transaction_id FROM shopee.shopee_payment_module_provision_id_db__provision_v2_tab
), 
pmt_txn_prov AS (
    SELECT DATE(from_unixtime(ctime-(3600))) as payment_date, 
    b.channel_id as channel_id,
    a.client_id as client_id,
    h.channel_id as provision_channel_id,
    a.transaction_id, b.status,
    MAX(CASE WHEN b.status = 0 THEN 1 ELSE 0 END) OVER(PARTITION BY a.transaction_id) AS initial_condition,
    MAX(CASE WHEN b.status = 2 THEN 1 ELSE 0 END) OVER(PARTITION BY a.transaction_id) AS payment_init_condition,
    MAX(CASE WHEN b.status = 6 THEN 1 ELSE 0 END) OVER(PARTITION BY a.transaction_id) AS user_processing_condition,
    MAX(CASE WHEN b.status = 22 THEN 1 ELSE 0 END) OVER(PARTITION BY a.transaction_id) AS failed_condition,
    MAX(CASE WHEN b.status = 50 THEN 1 ELSE 0 END) OVER(PARTITION BY a.transaction_id) AS cancel_condition,
    MAX(CASE WHEN b.status = 54 THEN 1 ELSE 0 END) OVER(PARTITION BY a.transaction_id) AS expired_condition,
    MAX(CASE WHEN b.status IN (20,24,39,40) THEN 1 ELSE 0 END) OVER(PARTITION BY a.transaction_id) AS success_condition,
    MAX(CASE WHEN b.status NOT IN (20,24,39,40) THEN 1 ELSE 0 END) OVER(PARTITION BY a.transaction_id) AS churn_condition
    FROM pmt b
    LEFT JOIN txn a
    ON a.transaction_id = b.transaction_id
    LEFT JOIN prov h
    ON a.transaction_id = h.transaction_id
    GROUP BY 1,2,3,4,5,6
), 
mapping AS (
    SELECT client_id, client_name, description AS client_desc, 
    CASE WHEN mapping = 'ShopeePlay' THEN 'Shopee Play' ELSE mapping END AS client_mapping
    FROM shopee.shopee_regional_bi_team__spm_transaction_client_mapping
    WHERE country = 'ID'
),
txn_data AS (
    SELECT
        payment_date, channel_id, client_id, provision_channel_id,
        SUM(CASE WHEN initial_condition > 0 THEN 1 ELSE 0 END) AS initial_count,
        SUM(CASE WHEN payment_init_condition > 0 THEN 1 ELSE 0 END) AS payment_init_count,
        SUM(CASE WHEN user_processing_condition > 0 THEN 1 ELSE 0 END) AS user_processing_count,
        SUM(CASE WHEN failed_condition > 0 THEN 1 ELSE 0 END) AS failed_count,
        SUM(CASE WHEN expired_condition > 0 THEN 1 ELSE 0 END) AS expired_count,
        SUM(CASE WHEN cancel_condition > 0 THEN 1 ELSE 0 END) AS cancel_count,
        SUM(CASE WHEN success_condition > 0 THEN 1 ELSE 0 END) AS success_count,
        SUM(CASE WHEN churn_condition > 0 THEN 1 ELSE 0 END) AS churn_count,
        COUNT(*) AS total_txn_count
    FROM(
        SELECT 
            payment_date, channel_id, client_id, provision_channel_id,
            transaction_id,
            SUM(initial_condition) AS initial_condition,
            SUM(payment_init_condition) AS payment_init_condition,
            SUM(user_processing_condition) AS user_processing_condition,
            SUM(failed_condition) AS failed_condition,
            SUM(expired_condition) AS expired_condition,
            SUM(cancel_condition) AS cancel_condition,
            SUM(success_condition) AS success_condition,
            SUM(churn_condition) AS churn_condition
        FROM pmt_txn_prov 
        GROUP BY 1,2,3,4,5
    )
    GROUP BY 1,2,3,4
    ORDER BY 1,2,3,4
),
txn_mapped AS (
    SELECT
        payment_date, b.client_mapping, 
        initial_count, payment_init_count, user_processing_count, failed_count, expired_count,
        cancel_count, success_count, churn_count, total_txn_count
    FROM txn_data a 
    JOIN mapping b ON CAST(a.client_id AS VARCHAR) = b.client_id
    GROUP BY 1,2,3,4,5,6,7,8,9,10,11
)


SELECT * FROM txn_mapped