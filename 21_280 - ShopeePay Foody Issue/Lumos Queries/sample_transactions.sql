WITH pmt AS (
    SELECT payment_id, status, channel_id, amount, transaction_id, ctime
    FROM shopee.shopee_payment_module_payment_id_db__payment_v2_tab 
    WHERE payment_type = 4 --payment type = PAYMENT
    AND DATE(from_unixtime(ctime-(3600))) BETWEEN DATE '2021-08-02' AND DATE '2021-08-23'
    AND channel_id = 8001400 -- ID-ShopeePay
), 
txn AS (
    SELECT client_id, CAST(transaction_id AS BIGINT) AS transaction_id FROM shopee.shopee_payment_module_transaction_id_db__transaction_v2_tab
), 
prov AS (
    SELECT channel_id, CAST(transaction_id AS BIGINT) AS transaction_id FROM shopee.shopee_payment_module_provision_id_db__provision_v2_tab
), 
pmt_txn_prov AS (
    SELECT DATE(from_unixtime(ctime-(3600))) as payment_date, 
    b.channel_id as channel_id,
    a.client_id as client_id,
    h.channel_id as provision_channel_id,
    a.transaction_id, b.status,
    MAX(CASE WHEN b.status = 6 THEN 1 ELSE 0 END) OVER(PARTITION BY a.transaction_id) AS user_processing_condition,
    MAX(CASE WHEN b.status = 50 THEN 1 ELSE 0 END) OVER(PARTITION BY a.transaction_id) AS cancel_condition,
    MAX(CASE WHEN b.status = 54 THEN 1 ELSE 0 END) OVER(PARTITION BY a.transaction_id) AS expired_condition
    FROM pmt b
    LEFT JOIN txn a
    ON a.transaction_id = CAST(b.transaction_id AS BIGINT)
    LEFT JOIN prov h
    ON a.transaction_id = h.transaction_id
    GROUP BY 1,2,3,4,5,6
), 
mapping AS (
    SELECT client_id, client_name, description AS client_desc, 
    CASE WHEN mapping = 'ShopeePlay' THEN 'Shopee Play' ELSE mapping END AS client_mapping
    FROM shopee.shopee_regional_bi_team__spm_transaction_client_mapping
    WHERE country = 'ID'
),
txn_data AS (
    SELECT 
        payment_date, channel_id, client_id, provision_channel_id,
        transaction_id,
        SUM(user_processing_condition) AS user_processing_condition,
        SUM(expired_condition) AS expired_condition,
        SUM(cancel_condition) AS cancel_condition
    FROM pmt_txn_prov 
    GROUP BY 1,2,3,4,5
)

(SELECT payment_date, channel_id client_id, provision_channel_id, transaction_id, 'Processing' AS status
FROM txn_data
WHERE user_processing_condition > 0
LIMIT 10)
UNION ALL
(SELECT payment_date, channel_id client_id, provision_channel_id, transaction_id, 'Expired' AS status
FROM txn_data
WHERE expired_condition > 0
LIMIT 10)
UNION ALL
(SELECT payment_date, channel_id client_id, provision_channel_id, transaction_id, 'Cancelled' AS status
FROM txn_data
WHERE cancel_condition > 0
LIMIT 10)