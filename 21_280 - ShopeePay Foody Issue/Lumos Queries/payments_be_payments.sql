WITH exchangeRate AS (
    SELECT date_id, exchange_rate
    FROM shopee.dim_exrate
    WHERE country = 'ID'
    AND grass_date >= CURRENT_DATE - interval '30' day
), pmt AS (
    SELECT payment_id, status, channel_id, amount, transaction_id, ctime
    FROM shopee.shopee_payment_module_payment_id_db__payment_v2_tab 
    WHERE payment_type = 4 --payment type = PAYMENT
    AND DATE(from_unixtime(ctime-(3600))) BETWEEN DATE '2021-08-07' AND DATE '2021-08-23'
    AND channel_id = 8001400 -- ID-ShopeePay 
), 
txn AS (
    SELECT client_id, transaction_id FROM shopee.shopee_payment_module_transaction_id_db__transaction_v2_tab
), 
prov AS (
    SELECT channel_id, transaction_id FROM shopee.shopee_payment_module_provision_id_db__provision_v2_tab
), 
pmt_txn_prov AS (
    SELECT DATE(from_unixtime(ctime-(3600))) as payment_date, 
    b.channel_id as channel_id,
    a.client_id as client_id,
    h.channel_id as provision_channel_id,
    COUNT(distinct b.payment_id) as total_payment_no,
    COUNT(distinct case when b.status in (20,24,39,40) then b.payment_id else null end) as successful_payment_no,
    COUNT(distinct case when b.status not in (20,24,39,40) then b.payment_id else null end) as churn_payment_no,
    SUM(CASE when b.status in (20,24,39,40) then b.amount/100000.00 else null end) as successful_payment_amount_local_currency,
    SUM(CASE when b.status not in (20,24,39,40) then b.amount/100000.00 else null end) as churn_payment_amount_local_currency
    FROM pmt b
    LEFT JOIN txn a
    ON a.transaction_id = b.transaction_id
    LEFT JOIN prov h
    ON a.transaction_id = h.transaction_id

    GROUP BY 1,2,3,4
),
mapping AS (
    SELECT client_id, client_name, description AS client_desc, 
    CASE WHEN mapping = 'ShopeePlay' THEN 'Shopee Play' ELSE mapping END AS client_mapping
    FROM shopee.shopee_regional_bi_team__spm_transaction_client_mapping
    WHERE country = 'ID'
),
agg AS (
    SELECT * FROM pmt_txn_prov a
    LEFT JOIN exchangeRate as e 
    ON a.payment_date = e.date_id
)

SELECT 
    a.payment_date, a.channel_id, a.client_id, a.provision_channel_id,
    b.client_name, b.client_desc, b.client_mapping, 
    SUM(total_payment_no) AS total_payment_no,
    SUM(successful_payment_no) AS successful_payment_no,
    SUM(churn_payment_no) AS churn_payment_no,
    (CAST(SUM(successful_payment_amount_local_currency) AS DOUBLE) / CAST(MIN(exchange_rate) AS DOUBLE)) AS successful_payment_amount_usd,
    (CAST(SUM(churn_payment_amount_local_currency) AS DOUBLE) / CAST(MIN(exchange_rate) AS DOUBLE)) AS churn_payment_amount_usd
FROM agg a
JOIN mapping b ON CAST(a.client_id AS VARCHAR) = b.client_id
GROUP BY 1,2,3,4,5,6,7
ORDER BY 1,3,4