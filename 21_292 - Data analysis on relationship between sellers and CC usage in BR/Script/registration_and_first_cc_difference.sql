WITH sellerList AS ( -- Find registration time
    SELECT 
        --user_id, 
        shop_id, create_datetime
    FROM shopee.user_mart_dim_shop
    WHERE 
        grass_region = 'BR'
        AND grass_date = (SELECT MAX(grass_date) FROM shopee.user_mart_dim_shop)
        AND is_cb_shop = 0 -- local sellers
        AND (create_datetime - interval 3 hour) >= (current_timestamp - interval 3 hour - interval 1 year)
), earliest_cc AS (
    SELECT
        checkout_info.order.shopid[0] AS shop_id, MIN(from_unixtime(ctime - (11*3600))) AS earliest_cc
    FROM shopee.shopee_checkout_v2_db__checkout_v2_tab
    WHERE 
        grass_region = 'BR'
        AND from_unixtime(ctime - (11*3600)) >= current_timestamp - interval '11' hour - interval '1' year -- GMT+8 to GMT-3 (BR)
        AND status = 4 -- Successful payments
        AND CAST(checkout_info.checkout_payment_info.spm_channel_id AS BIGINT) IN (10006200, 10006201)
    GROUP BY 1
), summary_table AS (
    SELECT DATEDIFF(b.earliest_cc, a.create_datetime) AS duration_diff
    FROM sellerList a
    RIGHT JOIN earliest_cc b ON a.shop_id = b.shop_id
)

SELECT 
    AVG(duration_diff) AS avg, 
    MIN(duration_diff) AS min, 
    APPROX_PERCENTILE(duration_diff, 0.25) AS Q1,
    APPROX_PERCENTILE(duration_diff, 0.5) AS Median,
    APPROX_PERCENTILE(duration_diff, 0.75) AS Q3,
    MAX(duration_diff) AS max,
    STDDEV(duration_diff) AS std,
    COUNT(duration_diff) AS count
FROM summary_table