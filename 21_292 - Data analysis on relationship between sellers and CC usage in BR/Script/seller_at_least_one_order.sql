-- Amount of distinct local sellers in BR who registered within 1 year from today and received at least one order

SELECT 
    COUNT(DISTINCT checkout_info.order.shopid[0]) AS shop_count
FROM shopee.shopee_checkout_v2_db__checkout_v2_tab
WHERE grass_region = 'BR'
-- AND from_unixtime(ctime - (11*3600)) >= current_timestamp - interval '1' year - interval '11' hour -- GMT+8 to GMT-3 (BR)
    AND status = 4 -- Successful
    AND from_unixtime(ctime - (11*3600))
       BETWEEN (DATE '2020-08-01' - interval 11 hour) AND (DATE '2021-07-31' - interval 11 hour)