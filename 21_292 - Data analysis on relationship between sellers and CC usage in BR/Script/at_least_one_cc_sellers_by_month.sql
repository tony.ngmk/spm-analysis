SELECT 
    CONCAT_WS('-', year, month) AS year_month, 
    shopCount, 
    SUM(shopCount) OVER(ORDER BY YEAR, MONTH ROWS BETWEEN UNBOUNDED PRECEDING AND CURRENT ROW) AS cumSum
FROM (
        SELECT 
            YEAR(from_unixtime(ctime - (11*3600))) AS year, MONTH(from_unixtime(ctime - (11*3600))) AS month,
            COUNT(DISTINCT shop_id) AS shopCount
        FROM (
        SELECT checkout_info.order.shopid[0] shop_id, MIN(ctime) AS ctime
        FROM shopee.shopee_checkout_v2_db__checkout_v2_tab
        WHERE 
            grass_region = 'BR'
            AND status = 4 -- Successful
            AND CAST(checkout_info.checkout_payment_info.spm_channel_id AS BIGINT) IN (10006200, 10006201)
            AND from_unixtime(ctime - (11*3600)) <= (DATE '2021-07-31' - interval 11 hour)
            -- AND from_unixtime(ctime - (11*3600)) >= current_timestamp - interval '1' year - interval '11' hour -- GMT+8 to GMT-3 (BR)
        GROUP BY 1
    )
        GROUP BY 1,2
        ORDER BY 1,2
)