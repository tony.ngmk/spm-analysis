SELECT 
    CONCAT_WS('-', year, month) AS year_month, 
    shopCount, 
    SUM(shopCount) OVER(ORDER BY YEAR, MONTH ROWS BETWEEN UNBOUNDED PRECEDING AND CURRENT ROW) AS cumSum
FROM (
    SELECT 
        YEAR(create_datetime - interval 3 hour) AS year, MONTH(create_datetime - interval 3 hour) AS month,
        COUNT(DISTINCT shop_id) AS shopCount
    FROM shopee.user_mart_dim_shop
    WHERE 
        grass_region = 'BR'
        AND grass_date = (SELECT MAX(grass_date) FROM shopee.user_mart_dim_shop)
        AND is_cb_shop = 0 -- local sellers
        -- AND (create_datetime - interval 3 hour) >= (current_timestamp - interval 11 hour - interval 1 year)
    GROUP BY 1,2
    ORDER BY 1,2
)